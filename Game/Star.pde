class Star
{
  PVector position;
  float diamater;
  
  Star(float tempDiamater)
  {
    position = new PVector(random(width), random(height));
    diamater = tempDiamater;
  }
  
  void update() 
  {
  }
  
  void display() 
  {
    //set the fill color to a random color to create a flickering effect
    fill(random(100,255));
    ellipse(position.x, position.y, diamater, diamater);
  }

}
