class Player{
 PVector position;
 
 Player(){
   position = new PVector();
 }
  
  void update()
  {
    position.set(mouseX, mouseY); 
  }
  
  void display()
  {
    rectMode(CENTER);
    
    fill(255);
    
    rect(mouseX, mouseY, 40, 40);
    
    fill(255,0,0);
    ellipse(pmouseX, pmouseY, 20, 20);
  }
  
}
