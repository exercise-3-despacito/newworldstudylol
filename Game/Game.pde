Player playerOne;
int starCount = 50;    //number of stars to instantiate
Star[] star = new Star[starCount];
float minDiamater=3;
float maxDiamater=8;

void setup()
{
  // set the canvas size
  size(400,400);
  
  //set the frame rate
  frameRate(60);
  
  playerOne = new Player();
  // cinstantiate stars to fill the stars array
  for(int i=0; i< star.length; i++)
  {
    star[i] = new Star(random(minDiamater,maxDiamater));
  }
}

void draw()
{

  playerOne.update();
 
  //update all the stars
  for(int i=0; i<star.length;i++)
  {
    star[i].update();
  }
  
  //blank the background
  background(0);
  playerOne.display();
  
  //draw a rectangle
  rect(width/2, height/2, 40,40);
  //display all the stars
  for(int i=0; i<star.length;i++)
  {
    star[i].display();
  }
  
  //draw a rectangle
  fill(255);
  rect(width/2, height/2, 40,40);
}
